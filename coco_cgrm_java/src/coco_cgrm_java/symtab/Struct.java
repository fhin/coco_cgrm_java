package coco_cgrm_java.symtab;

import java.util.Collections;
import java.util.Map;

import coco_cgrm_java.impl.StructImpl;

/**
 * MicroJava Type Structures: A type structure stores the type attributes of a
 * declared type.
 */
public abstract class Struct {
	/** Possible codes for structure kinds. */
	public enum Kind {
		None, Int, Char, Bool, Arr, Ptr, Class
	}

	/** Kind of the structure node. */
	public final Kind kind;
	
	public int size;
	
	/** Arr, Rec: number of elements/fields. */
	public int n;
	
	/** Only for Arr: Type of the array elements. For Pointer base type */
	public final StructImpl elemType;
	/** Only for Class: First element of the linked list of local variables. */
	public Map<String, Obj> fields = Collections.emptyMap();

	protected Struct(Kind kind, StructImpl elemType, int size) {
		this.kind = kind;
		this.elemType = elemType;
		this.size = size;
	}
	
	protected Struct(Kind kind, StructImpl elemType) {
		this.kind = kind;
		this.elemType = elemType;
	}
	
	public Struct(Kind kind) {
		this(kind, null);
		int s = 0;
		switch (kind){
		case Int:
			s = 4;
			break;
		case Char:
			s = 1;
			break;
		case Bool:
			s = 1;
			break;
		default:
			break;
		}
		this.size = s;
	}

	/**
	 * Creates a new array structure with a specified element type.
	 */
	public Struct(StructImpl elemType) {
		this(Kind.Arr, elemType);
		this.size = elemType.size;
	}

	/**
	 * Retrieves the field <code>name</code>.
	 */
	public Obj findField(String name) {
		return fields.get(name);
	}

	/** Only for Class: Number of fields. */
	public int nrFields() {
		return fields.size();
	}

	public abstract boolean compatibleWith(StructImpl other);

	public abstract boolean assignableTo(StructImpl dest);
}
