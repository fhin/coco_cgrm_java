package coco_cgrm_java.symtab;

import java.util.Collections;
import java.util.Map;

import coco_cgrm_java.impl.StructImpl;

/**
 * MicroJava Symbol Table Objects: Every named object in a program is stored in
 * an <code>Obj</code> node. Every scope has a list of objects declared within
 * it.
 */
public class Obj {
	/** Possible codes for object kinds. */
	public enum Kind {
		Con, Var, Type, ValPar, VarPar, Field, Meth, Prog
	}

	/** Kind of the object node. */
	public final Kind kind;
	/** Name of the object node. */
	public final String name;
	/** Type of the object node. */
	public StructImpl type;
	/** Only for Con: Value of the constant. */
	public int val;
	/** Only for Var, Meth: Offset of the element. */
	public int adr;
	/** Only for Var / Meth: Declaration level (0..global, 1..local) / Nesting level */
	public int level;
	/** Only for Meth: Number of parameters. */
	public int nPars;
	/** Only for Meth: Size of variables (locals) / Size of parameters */
	public int varSize, parSize;
	/** Only for Meth / Prog: List of local variables / global declarations. */
	public Map<String, Obj> locals = Collections.emptyMap();

	public Obj(Kind kind, String name, StructImpl type) {
		this.kind = kind;
		this.name = name;
		this.type = type;
	}
}

