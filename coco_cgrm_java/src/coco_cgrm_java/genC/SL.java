package coco_cgrm_java.genC;

import coco_cgrm_java.code.Code;

public class SL {
	public static void main (String[] args) {
		if (args.length > 0){
			String file = args[0];
			Scanner scanner = new Scanner(file);
			Parser parser = new Parser(scanner);
			parser.Parse();
			System.out.println(parser.errors.count + " errors detected"); 
			Code.WriteInstToFile(args[1]);
		}
		else {
			System.out.println("No file to parse given, check program arguments");
		}
	}
}
