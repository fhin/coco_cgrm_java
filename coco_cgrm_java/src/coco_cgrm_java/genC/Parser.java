package coco_cgrm_java.genC;

import coco_cgrm_java.symtab.*;
import coco_cgrm_java.symtab.Obj.Kind;
import coco_cgrm_java.impl.*;
import coco_cgrm_java.code.*;
import coco_cgrm_java.code.Enums.CmpConsts;
import coco_cgrm_java.code.Enums.DyadicOps;
import coco_cgrm_java.code.Enums.Reg;
import coco_cgrm_java.code.Operand.OpdKind;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List; 
import java.util.Map; 



public class Parser {
	public static final int _EOF = 0;
	public static final int _ident = 1;
	public static final int _number = 2;
	public static final int _charCon = 3;
	public static final int maxT = 34;

	static final boolean _T = true;
	static final boolean _x = false;
	static final int minErrDist = 2;

	public Token t;    // last recognized token
	public Token la;   // lookahead token
	int errDist = minErrDist;
	
	public Scanner scanner;
	public Errors errors;

	public TabImpl tab;
 public Code gen;
 public Operand retOp; // Stores the register in which the return value of a method is safed



	public Parser(Scanner scanner) {
		this.scanner = scanner;
		errors = new Errors();
	}

	void SynErr (int n) {
		if (errDist >= minErrDist) errors.SynErr(la.line, la.col, n);
		errDist = 0;
	}

	public void SemErr (String msg) {
		if (errDist >= minErrDist) errors.SemErr(t.line, t.col, msg);
		errDist = 0;
	}
	
	void Get () {
		for (;;) {
			t = la;
			la = scanner.Scan();
			if (la.kind <= maxT) {
				++errDist;
				break;
			}

			la = t;
		}
	}
	
	void Expect (int n) {
		if (la.kind==n) Get(); else { SynErr(n); }
	}
	
	boolean StartOf (int s) {
		return set[s][la.kind];
	}
	
	void ExpectWeak (int n, int follow) {
		if (la.kind == n) Get();
		else {
			SynErr(n);
			while (!StartOf(follow)) Get();
		}
	}
	
	boolean WeakSeparator (int n, int syFol, int repFol) {
		int kind = la.kind;
		if (kind == n) { Get(); return true; }
		else if (StartOf(repFol)) return false;
		else {
			SynErr(n);
			while (!(set[syFol][kind] || set[repFol][kind] || set[0][kind])) {
				Get();
				kind = la.kind;
			}
			return StartOf(syFol);
		}
	}
	
	void SL() {
		Expect(4);
		tab = new TabImpl();
		tab.openScope(); 
		int offset = 0;
		Code.SetProgramStartAdr(); 
		
		while (la.kind == 8 || la.kind == 12) {
			offset = Decleration(offset);
		}
		Expect(5);
		Code.FixupStartAdr(Code.pc); 
		StatSeq(tab.noObj);
		Expect(6);
		Expect(7);
		tab.closeScope();
		Code.Close(); 
	}

	int  Decleration(int inOffset) {
		int  offset;
		offset = 0; 
		if (la.kind == 8) {
			offset = VarDecl(0, inOffset);
		} else if (la.kind == 12) {
			ProcDecl();
		} else SynErr(35);
		return offset;
	}

	void StatSeq(Obj procObj) {
		Statement(procObj);
		while (la.kind == 10) {
			Get();
			Statement(procObj);
		}
	}

	int  VarDecl(int level, int initOffs) {
		int  offset;
		List<String> names; StructImpl type; Obj o; 
		offset = initOffs; 
		
		Expect(8);
		while (la.kind == 1) {
			names = IdList();
			Expect(9);
			type = Type();
			Expect(10);
			for (String name : names) {
			if (tab.find(name) != tab.noObj) SemErr("Variable already declared");
			else {
			o = tab.insert(Kind.Var, name, type);
			o.level = level;
			if (level == 0) {
			o.adr = o.type.size + offset;
			offset += o.type.size;
			}
			else {
			o.adr = -o.type.size + offset;
			offset -= o.type.size;
			}
			}
			}
			
		}
		return offset;
	}

	void ProcDecl() {
		Obj procObj; String name; StructImpl methType; int varOffset = 0; 
		Expect(12);
		name = Ident();
		if (tab.find(name) != tab.noObj) SemErr("cannot declare method twice"); 
		methType = Tab.noType;
		procObj = tab.insert(Kind.Meth, name, methType);
		procObj.adr = Code.pc;
		tab.openScope();
		
		if (la.kind == 13) {
			methType = Parameters(procObj.level);
		}
		Expect(10);
		procObj.type = methType;	
		procObj.nPars = tab.curScope.locals().size();
		for (Map.Entry<String, Obj> kvPair : tab.curScope.locals().entrySet()) {
		Obj o = kvPair.getValue();
		if (o != null) {
		procObj.parSize += o.type.size;
		if (o.type == Tab.charType) {
		procObj.parSize += 3; // Stack is 32 bit aligned
		}
		} 
		}
		
		while (la.kind == 8) {
			varOffset = VarDecl(1, varOffset);
		}
		procObj.locals = tab.curScope.locals();
		int nSkippedLocals = 0;
		if (procObj.locals.size() > 0) {
		for(Map.Entry<String, Obj> kvPair : procObj.locals.entrySet()) {
		if (nSkippedLocals < procObj.nPars) {
		nSkippedLocals++;
		}
		else {
		Obj o = kvPair.getValue();
		if (o != null && o.level == 1 && o.kind == Kind.Var) {
		procObj.varSize += o.type.size;
		}
		}
		}
		}
		Code.PutENTER(procObj.varSize, 0);
		
		if (la.kind == 5) {
			Get();
			StatSeq(procObj);
		}
		Expect(6);
		Expect(1);
		name = t.val;
		if (!name.equals(procObj.name)) SemErr ("closing name does not match declared method name");
		tab.closeScope(); 
		// If StatSeq has no RETURN statement put procedure epilogue
		if (procObj.val != 1) {
		Code.PutLEAVE();
		Code.PutRET(procObj.parSize);
		}
		
		Expect(10);
	}

	List<String>  IdList() {
		List<String>  names;
		String name; names = new ArrayList<String>(); 
		name = Ident();
		names.add(name); 
		while (la.kind == 11) {
			Get();
			name = Ident();
			names.add(name); 
		}
		return names;
	}

	StructImpl  Type() {
		StructImpl  type;
		Expect(1);
		if (t.val.equals("INTEGER")) { 
		type = Tab.intType;
		}
		else if (t.val.equals("CHAR")) {
		type = Tab.charType;
		}
		else {
		type = Tab.noType;
		SemErr("Type can only be INTEGER or CHAR");
		} 
		
		return type;
	}

	String  Ident() {
		String  name;
		Expect(1);
		name = t.val; 
		return name;
	}

	StructImpl  Parameters(int nestingLvl) {
		StructImpl  retType;
		StructImpl type = Tab.noType; int offset = 0; 
		Expect(13);
		if (la.kind == 1 || la.kind == 8) {
			offset = Param(nestingLvl, 0, false);
			while (la.kind == 10) {
				Get();
				offset = Param(nestingLvl, offset, true);
			}
		}
		Expect(14);
		if (la.kind == 9) {
			Get();
			type = Type();
		}
		retType = type; 
		return retType;
	}

	int  Param(int procNestingLvl, int initOffset, boolean alignCharAdr) {
		int  offset;
		List<String> names; StructImpl type;
		boolean isVar = false; 
		int initOffS = 0;
		if (initOffset > 0) {
		initOffS = initOffset;
		} 
		else {
		initOffS = procNestingLvl > 0 ? 12 : 8;
		}
		int nextOffs = 0; 
		if (la.kind == 8) {
			Get();
			isVar = true; 
		}
		names = IdList();
		Expect(9);
		type = Type();
		for (String name : names) {
		if (tab.find(name) != tab.noObj) SemErr("Parameter already declared");
		else {
		Obj paramObj = null;
		if (isVar) paramObj = tab.insert(Kind.VarPar, name, type);
		else paramObj = tab.insert(Kind.Var, name, type);
		if (type == Tab.charType){
		if (!alignCharAdr){
		alignCharAdr = true;
		}
		else {
		nextOffs += 3; // Stack is 32 bit aligned, loading a char parameter requires this additional offset
		}
		}
		paramObj.adr = initOffS + nextOffs;
		paramObj.level = 1;
		nextOffs += type.size; 
		} 		
		}
		offset = nextOffs + initOffS;
		
		return offset;
	}

	void Statement(Obj procObj) {
		Operand x, y; String name; Obj obj; x = null; 
		if (StartOf(1)) {
			if (la.kind == 1) {
				name = Ident();
				obj = tab.find(name);
				
				if (la.kind == 21) {
					Get();
					if (obj.kind == Kind.Var || obj.kind == Kind.VarPar) {
					x = Code.VarOpd(obj);
					}
					else {
					SemErr("Can only assign to a variable or parameter");
					}
					
					y = Expression();
					if (obj == tab.noObj) { 
					SemErr("Can only assign to a declared variable");
					} 
					if (!y.type.assignableTo(obj.type)) {
					SemErr("incompatible types");
					}  
					if (x.type.size > 4) {
					SemErr("TODO: ");
					}
					if (y.kind == OpdKind.Proc) {
					if (y.type != Tab.noType && retOp != null) {
					Code.PutMOV(x, retOp);
					}
					Code.FreeOpd(retOp);
					retOp = null;
					}	
					else {
					//Code.Load(y);
					Code.PutMOV(x, y);
					}
					Code.FreeAllRegs();
					
				} else if (la.kind == 13) {
					ActParameters(name);
				} else SynErr(36);
			} else if (la.kind == 17) {
				IfStat(procObj);
			} else if (la.kind == 15) {
				WhileStat(procObj);
			} else {
				Get();
				if (StartOf(2)) {
					x = Expression();
					if (!x.type.assignableTo(procObj.type)){
					SemErr("Type of return statement does no match declared method type");
					} 
				}
				Code.PutLEAVE();
				if (procObj != tab.noObj) {
				if (x != null) {
				retOp = x;
				}
				Code.PutRET(procObj.parSize);
				}
				else {
				Code.PutRET(0);
				} 
				procObj.val = 1;
				
			}
		}
	}

	void WhileStat(Obj procObj) {
		Operand x; 
		Expect(15);
		int start = Code.pc; 
		int end = 0; 
		x = Condition();
		if (x.kind != OpdKind.Cond) SemErr("TODO: Cond Err");
		if (x.tJmpL.size() > 1) {
		x.tJmpL.add(end);
		}
		x.tJmpL = Code.PutJcc(x.lastCmpOp, CmpConsts.F_JMP.val, 
		CmpConsts.SIGNED.val, x.tJmpL);
		end = x.tJmpL.get(0);
		
		Expect(16);
		StatSeq(procObj);
		Expect(6);
		start = Code.PutJMP(start);
		Code.Fixup(end); 
	}

	Operand  Condition() {
		Operand  x;
		Operand y; int relOp; 
		x = Expression();
		relOp = Relop();
		x.lastCmpOp = relOp; 
		y = Expression();
		Code.Load(x);
		Code.PutDyadic(DyadicOps.CMP.opCode, x, y);
		Code.FreeOpd(x); Code.FreeOpd(y);
		x.kind = OpdKind.Cond; x.type = Tab.boolType;
		x.tJmpL = new ArrayList<Integer>(); x.fJmpL = new ArrayList<Integer>(); 
		x.tJmpL.add(0); x.fJmpL.add(0); 
		return x;
	}

	void IfStat(Obj procObj) {
		Operand x; int end = 0;
		Expect(17);
		x = Condition();
		x.fJmpL = Code.PutJcc(x.lastCmpOp, CmpConsts.F_JMP.val,
		CmpConsts.SIGNED.val, x.fJmpL);
		x.tJmpL = Code.Fixup(x.tJmpL);
		
		Expect(18);
		StatSeq(procObj);
		while (la.kind == 19) {
			Get();
			end = Code.PutJMP(end);
			x.fJmpL = Code.Fixup(x.fJmpL);
			
			x = Condition();
			if (x.kind != OpdKind.Cond) SemErr("TODO: Cond Err");
			x.fJmpL = Code.PutJcc(x.lastCmpOp, CmpConsts.F_JMP.val,
			CmpConsts.SIGNED.val, x.fJmpL);
			x.tJmpL = Code.Fixup(x.tJmpL);
			
			Expect(18);
			StatSeq(procObj);
		}
		if (la.kind == 20) {
			Get();
			end = Code.PutJMP(end);
			x.fJmpL = Code.Fixup(x.fJmpL);
			
			StatSeq(procObj);
		}
		Expect(6);
		Code.Fixup(end); 
	}

	Operand  Expression() {
		Operand  x;
		Operand y = null; int op = Enums.Ops.NONE.ascii_val; 
		if (la.kind == 29 || la.kind == 30) {
			op = Addop();
		}
		x = Term();
		if (x.type == Tab.intType && op == Enums.Ops.MINUS.ascii_val) {
		if (x.kind == OpdKind.Con) {
		x.val = -x.val;
		}
		else {
		Code.PutNEG(x);
		}
		}
		
		while (la.kind == 29 || la.kind == 30) {
			op = Addop();
			y = Term();
			if (y == null) SemErr("Addop needs two operands, second operand is missing");
			if (x.type == Tab.intType && y.type == Tab.intType) {
			if (op != Enums.Ops.NONE.ascii_val) {
			Code.GenOp(op, x, y);
			}
			}
			else { 
			SemErr("Both operands of Addop need to be of type int");
			}
			
		}
		return x;
	}

	void ActParameters(String methName) {
		Operand x = null; Obj procObj; int actPars = 0;
		List<Operand> params = new ArrayList<Operand>();
		procObj = tab.find(methName);
		if (procObj == tab.noObj) SemErr("No method declared with name " + methName);
		if (procObj.kind != Kind.Meth) SemErr("");
		boolean isGlobal = tab.isPredefinedProc(procObj);
		boolean isGlobalInline = tab.isPredefinedInlineProc(procObj); 
		Iterator<Map.Entry<String, Obj>> localIt = procObj.locals.entrySet().iterator(); 
		Expect(13);
		if (isGlobal && !isGlobalInline) {
		Code.PutPUSH(Code.CreateTmpOpd(Reg.EDI.regNum));	// Save pointer to global data
		}
		
		if (StartOf(2)) {
			x = Expression();
			if (procObj.nPars == 0) SemErr("Method has no arguments defined but was called with some");
			actPars++;
			if (localIt.hasNext()) { 
			Obj paramObj = localIt.next().getValue();
			if (x == null) return;
			if (!x.type.assignableTo(paramObj.type)) {
			SemErr("Argument is not compatible with formal param");
			}
			else { 
			switch(paramObj.kind){
			case Var:
			params.add(x);
			break;
			case VarPar: 
			// TODO: Check if operand x is variable
			Operand tmp = Code.RegOpd(Code.any);
			Code.PutLEA(tmp, x);
			params.add(tmp);
			break;
			default:
			SemErr("Parameter can either be a value or variable");
			break;
			}
			}
			}	
			
			while (la.kind == 11) {
				Get();
				x = Expression();
				actPars++;
				if (localIt.hasNext()) {
				Obj paramObj = localIt.next().getValue();
				if (x == null) return;
				if (!x.type.assignableTo(paramObj.type)) SemErr("Argument is not compatible with formal param");
				else {
				switch(paramObj.kind){
				case Var:
				params.add(x);
				break;
				case VarPar: 
				// TODO: Check if operand x is variable
				Operand tmp = Code.RegOpd(Code.any);
				Code.PutLEA(tmp, x);
				params.add(tmp);
				break;
				default:
				SemErr("Parameter can either be a value or variable");
				break;
				}
				}
				}
				
			}
		}
		Expect(14);
		if (actPars > procObj.nPars) {
		SemErr("Too many actual params");
		}
		if (actPars < procObj.nPars) {
		SemErr("Too few actual params");
		} 
									if (!isGlobalInline) {
		for (int i = params.size() -1; i >= 0; i--){
		Code.PutPUSH(params.get(i));
		}
		Code.Call(procObj, procObj.level, isGlobal);
		if (isGlobal){
		Code.PutPOP(Reg.EDI.regNum);	// Restore pointer to global data
		}
		}
		else {
		if (params.size() > 0){
		retOp = Code.CallGlobInlineProc(params.get(0), procObj);
		}
		}
		
	}

	int  Relop() {
		int  x;
		x = Enums.Ops.NONE.ascii_val; 
		switch (la.kind) {
		case 23: {
			Get();
			x = Enums.CmpOps.EQ.idx; 
			break;
		}
		case 24: {
			Get();
			x = Enums.CmpOps.NEQ.idx; 
			break;
		}
		case 25: {
			Get();
			x = Enums.CmpOps.LT.idx; 
			break;
		}
		case 26: {
			Get();
			x = Enums.CmpOps.GT.idx; 
			break;
		}
		case 27: {
			Get();
			x = Enums.CmpOps.GEQ.idx; 
			break;
		}
		case 28: {
			Get();
			x = Enums.CmpOps.LEQ.idx; 
			break;
		}
		default: SynErr(37); break;
		}
		return x;
	}

	int  Addop() {
		int  x;
		x = Enums.Ops.NONE.ascii_val; 
		if (la.kind == 29) {
			Get();
			x = Enums.Ops.ADD.ascii_val; 
		} else if (la.kind == 30) {
			Get();
			x = Enums.Ops.MINUS.ascii_val; 
		} else SynErr(38);
		return x;
	}

	Operand  Term() {
		Operand  x;
		Operand y; int op = Enums.Ops.NONE.ascii_val; 
		x = Factor();
		while (la.kind == 31 || la.kind == 32 || la.kind == 33) {
			op = Mulop();
			y = Factor();
			if (x.type == Tab.intType && y.type == Tab.intType) {
			if (op != Enums.Ops.NONE.ascii_val) {
			Code.GenOp(op, x, y);
			}
			}
			else {
			SemErr("Both operands need to be of type int");
			} 
		}
		return x;
	}

	Operand  Factor() {
		Operand  x;
		String name; x = null;  
		if (la.kind == 1) {
			Get();
			name = t.val; 
			Obj o = tab.find(name);
			if (o != tab.noObj) {
			x = Code.VarOpd(o);
			}
			
			if (la.kind == 13) {
				ActParameters(name);
				if (o.kind == Kind.Meth && o.type != Tab.noType) {
				x = Code.RegOpd(Code.any);
				if (tab.isPredefinedInlineProc(o)) {
				if (o.parSize == 1){
				Operand constOp = Code.ConOpd(255);
				// Clear content of register to hold correct value after call to 
				// global inline function
				Code.PutLogicAND(retOp, constOp);	
				}
				Code.FreeOpd(x);
				x = retOp;
				}
				else {
				Code.PutMOV(x, retOp);
				Code.FreeOpd(retOp);
				}
				retOp = null;
				}
				
			}
		} else if (la.kind == 2) {
			Get();
			x = Code.ConOpd(Integer.valueOf(t.val)); 
		} else if (la.kind == 3) {
			Get();
			if (t.val.length() < 3){
			SemErr("Char constant has format '_'");
			x = Code.ConOpd(32); // Assign ascii value of space character
			}
			else {
			x = Code.ConOpd(t.val.charAt(1));					   	
			}
			x.type = Tab.charType;	
		} else if (la.kind == 13) {
			Get();
			x = Expression();
			Expect(14);
		} else SynErr(39);
		return x;
	}

	int  Mulop() {
		int  x;
		x = Enums.Ops.NONE.ascii_val; 
		if (la.kind == 31) {
			Get();
			x = Enums.Ops.MUL.ascii_val; 
		} else if (la.kind == 32) {
			Get();
			x = Enums.Ops.DIV.ascii_val; 
		} else if (la.kind == 33) {
			Get();
			x = Enums.Ops.MOD.ascii_val; 
		} else SynErr(40);
		return x;
	}



	public void Parse() {
		la = new Token();
		la.val = "";		
		Get();
		SL();
		Expect(0);

	}

	private static final boolean[][] set = {
		{_T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x},
		{_x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_T, _x,_T,_x,_x, _x,_x,_T,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x},
		{_x,_T,_T,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_x, _x,_x,_x,_x}

	};
} // end Parser


class Errors {
	public int count = 0;                                    // number of errors detected
	public java.io.PrintStream errorStream = System.out;     // error messages go to this stream
	public String errMsgFormat = "-- line {0} col {1}: {2}"; // 0=line, 1=column, 2=text
	
	protected void printMsg(int line, int column, String msg) {
		StringBuffer b = new StringBuffer(errMsgFormat);
		int pos = b.indexOf("{0}");
		if (pos >= 0) { b.delete(pos, pos+3); b.insert(pos, line); }
		pos = b.indexOf("{1}");
		if (pos >= 0) { b.delete(pos, pos+3); b.insert(pos, column); }
		pos = b.indexOf("{2}");
		if (pos >= 0) b.replace(pos, pos+3, msg);
		errorStream.println(b.toString());
	}
	
	public void SynErr (int line, int col, int n) {
		String s;
		switch (n) {
			case 0: s = "EOF expected"; break;
			case 1: s = "ident expected"; break;
			case 2: s = "number expected"; break;
			case 3: s = "charCon expected"; break;
			case 4: s = "\"PROGRAM\" expected"; break;
			case 5: s = "\"BEGIN\" expected"; break;
			case 6: s = "\"END\" expected"; break;
			case 7: s = "\".\" expected"; break;
			case 8: s = "\"VAR\" expected"; break;
			case 9: s = "\":\" expected"; break;
			case 10: s = "\";\" expected"; break;
			case 11: s = "\",\" expected"; break;
			case 12: s = "\"PROCEDURE\" expected"; break;
			case 13: s = "\"(\" expected"; break;
			case 14: s = "\")\" expected"; break;
			case 15: s = "\"WHILE\" expected"; break;
			case 16: s = "\"DO\" expected"; break;
			case 17: s = "\"IF\" expected"; break;
			case 18: s = "\"THEN\" expected"; break;
			case 19: s = "\"ELSIF\" expected"; break;
			case 20: s = "\"ELSE\" expected"; break;
			case 21: s = "\":=\" expected"; break;
			case 22: s = "\"RETURN\" expected"; break;
			case 23: s = "\"=\" expected"; break;
			case 24: s = "\"#\" expected"; break;
			case 25: s = "\"<\" expected"; break;
			case 26: s = "\">\" expected"; break;
			case 27: s = "\">=\" expected"; break;
			case 28: s = "\"<=\" expected"; break;
			case 29: s = "\"+\" expected"; break;
			case 30: s = "\"-\" expected"; break;
			case 31: s = "\"*\" expected"; break;
			case 32: s = "\"/\" expected"; break;
			case 33: s = "\"%\" expected"; break;
			case 34: s = "??? expected"; break;
			case 35: s = "invalid Decleration"; break;
			case 36: s = "invalid Statement"; break;
			case 37: s = "invalid Relop"; break;
			case 38: s = "invalid Addop"; break;
			case 39: s = "invalid Factor"; break;
			case 40: s = "invalid Mulop"; break;
			default: s = "error " + n; break;
		}
		printMsg(line, col, s);
		count++;
	}

	public void SemErr (int line, int col, String s) {	
		printMsg(line, col, s);
		count++;
	}
	
	public void SemErr (String s) {
		errorStream.println(s);
		count++;
	}
	
	public void Warning (int line, int col, String s) {	
		printMsg(line, col, s);
	}
	
	public void Warning (String s) {
		errorStream.println(s);
	}
} // Errors


class FatalError extends RuntimeException {
	public static final long serialVersionUID = 1L;
	public FatalError(String s) { super(s); }
}
