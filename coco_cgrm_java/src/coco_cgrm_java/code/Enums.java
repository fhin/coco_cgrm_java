package coco_cgrm_java.code;

public class Enums {
	public final static int[] jmpOpC = new int[] {
			0x74, // JE
			0x75, // JNE
			0x7C, // JL
			0x7D, // JGE
			0x7F, // JG
			0x7E, // JLE
			0x74, // JZ
			0x75, // JNZ
			0x72, // JB
			0x73, // JAE
			0x77, // JA
			0x76 // JBE
	};
	
	public enum CmpOps {
		EQ(0), NEQ(1), LT(2), GEQ(3), GT(4), LEQ(5);
		
		public final int idx;
		
		CmpOps(int idx){
			this.idx = idx;
		}
	}
	
	public enum CondHelper {
		TJMP(true), FJMP(false), SGND(true), UNSGND(false);
		
		public final boolean val;
		
		CondHelper(boolean val){
			this.val = val;
		}
	}
	
	public enum Ops {
		NONE(-1), MOD(37), MUL(42), ADD(43), MINUS(45), DIV(47);
		
		public final int ascii_val;
		
		Ops(int ascii){
			this.ascii_val = ascii;
		}
	}
	
	public enum Cmds {
		MUL_1B(0xF6), MUL(0xF7), NEG(0xF6), S_JMP(0xEB), L_JMP(0xE9),
		JE(0x74), JNE(0x75), JL(0x7C), JGE(0x7D), JG(0x7F), JLE(0x7E),
		JZ(0x74), JNZ(0x75), JB(0x72), JAE(0x73), JA(0x77), JBE(0x76),
		ENTER(0xC8), CALL(0xE8), CALL_INDIRECT(0xFF), LEAVE(0xC9), RET(0xC2), CWD(0x99), 
		IDIV(0xF6), AND_IMM(0x80), AND_MEM_TO_REG(0x22), AND_REG_TO_MEM(0x20),
		AND_IMM_TO_EAX(0x24), PUSH_IMM(0x68), PUSH_REG(0x50), PUSH_MEM(0xFF),
		POP(0x8F);
		
		public final int opCode;
		
		Cmds(int opCode){
			this.opCode = opCode;
		}
	}
	
	public enum CmpConsts {
		T_JMP(false), F_JMP(true), SIGNED(true), UNSIGNED(false);
		
		public final boolean val;
		
		CmpConsts(boolean val){
			this.val = val;
		}
	}
	
	public enum DyadicOps {
		ADD(0x0), ADC(0x10), SUB(0x28), SBB(0x18), AND(0x20), OR(0x8), XOR(0x30), CMP(0x38);
		
		public final int opCode;
		
		DyadicOps(int opCode){
			this.opCode = opCode;
		}
	}
	
	public enum RegAdressingMode {
		REL_ADR(0), MOD_NO_DISP(0), MOD_D8_DISP(1), MOD_D32_DISP(2), ABS_ADR(3), INDX_ADR(4), RM_D32_DISP(5); 
		
		public final int code;
		
		RegAdressingMode(int code){
			this.code = code;
		}
	};
	
	public enum Reg {
		EAX(0), ECX(1), EDX(2), EBX(3), ESP(4), EBP(5), ESI(6), EDI(7);
		
		public final int regNum;
		
		Reg(int regNum){
			this.regNum = regNum;
		}
	};
}
