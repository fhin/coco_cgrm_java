package coco_cgrm_java.impl;

import java.util.LinkedHashMap;

import coco_cgrm_java.symtab.Obj;
import coco_cgrm_java.symtab.Obj.Kind;
import coco_cgrm_java.symtab.Scope;
import coco_cgrm_java.symtab.Tab;

public final class TabImpl extends Tab {
	
	public TabImpl() {
		init();
	}

	private void init() {
		curLevel = -1;
		curScope = new Scope(null);
		insert(Kind.Type, "int", intType);
		insert(Kind.Type, "char", charType);
		insert(Kind.Type, "bool", boolType);
		insert(Kind.Con, "null", nullType);

		noObj = new Obj(Kind.Var, "$none", intType);

		chrObj = insert(Kind.Meth, "CHR", charType);
		addOneMethodParam("i", intType, chrObj, false);

		ordObj = insert(Kind.Meth, "ORD", intType);
		addOneMethodParam("ch", charType, ordObj, false);

		putObj = insert(Kind.Meth, "Put", noType);
		addOneMethodParam("e", charType, putObj, false);
		
		putLnObj = insert(Kind.Meth, "PutLn", noType);
		putLnObj.adr = 4;
	}

	private void addOneMethodParam(String paramName, StructImpl paramType,
			Obj obj, boolean isArray) {
		if (isArray) {
			paramType = new StructImpl(noType);
		}
		Obj paramObj = new Obj(Kind.Var, paramName, paramType);
		paramObj.level = 1;
		obj.locals = new LinkedHashMap<String, Obj>(); // Use a map that saves
														// insertion order
		obj.locals.put(paramName, paramObj);
		obj.nPars++;
		obj.parSize += paramType.size;
	}

	public void openScope() {
		Scope newScope = new Scope(curScope);
		curScope = newScope;
		curLevel++;
	}

	public void closeScope() {
		curScope = curScope.outer();
		curLevel--;
	}

	public Obj insert(Kind kind, String name, StructImpl type) {
		Obj obj = new Obj(kind, name, type);
		if (kind == Kind.Var || kind == Kind.VarPar) {
			obj.adr = curScope.nVars();
			obj.level = curLevel;
		} else if (!(kind == Kind.Con)) {
			obj.locals = new LinkedHashMap<String, Obj>();
			if (kind == Kind.Type) {
				obj.type.fields = new LinkedHashMap<String, Obj>();
			}
		}
		curScope.insert(obj);
		return obj;
	}

	public Obj find(String name) {
		Obj obj = curScope.findGlobal(name);
		if (obj == null) {
			return noObj;
		}
		return obj;
	}

	public Obj findField(String fieldName, StructImpl classStruct) {
		Obj field = classStruct.findField(fieldName);
		if (field == null) {
			return noObj;
		}
		return field;
	}
	
	public boolean isPredefinedProc(Obj proc, boolean onlyInline){
		if (proc == null || proc.level != 0) return false;
		Obj procObj = curScope.findGlobal(proc.name);
		if (procObj == null || procObj == noObj) return false;
		boolean isGlobalProc = false;
		if (onlyInline){
			switch(procObj.name){
				case "ORD": case "CHR": isGlobalProc = true; break;
				default: isGlobalProc = false; break;
			}	
		}
		else {
			switch(procObj.name){
				case "Put": case "PutLn": case "ORD": case "CHR": isGlobalProc = true; break;
				default: isGlobalProc = false; break;
			}
		}
		
		if (proc.name.equals(procObj.name) && procObj.level == 0 && isGlobalProc){
			return true;
		}
		return false;
	}
	
	public boolean isPredefinedProc(Obj proc){
		return isPredefinedProc(proc, false);
	}
	
	public boolean isPredefinedInlineProc(Obj proc){
		return isPredefinedProc(proc, true);
	}
}
