package coco_cgrm_java.impl;

import coco_cgrm_java.symtab.Struct;
import coco_cgrm_java.symtab.Tab;

public final class StructImpl extends Struct {

	private StructImpl(Kind kind, StructImpl elemType) {
		super(kind, elemType);
	}
	
	public StructImpl(Kind kind) {
		super(kind);
	}

	public StructImpl(StructImpl elemType) {
		super(elemType);
	}
	
	@Override
	public boolean compatibleWith(StructImpl other) {
		return (this.isRefType() && other == Tab.nullType)
				|| (other.isRefType() && this == Tab.nullType)
				|| checkTypeNames(this, other);
	}

	@Override
	public boolean assignableTo(StructImpl dest) {
		return (dest.isNumeric() && this.kind == Struct.Kind.Char)
				|| (dest.isNumeric() && this.isNumeric())
				|| (dest.kind == Struct.Kind.Char && this.kind == Struct.Kind.Char);
		/*
		return (dest.isRefType() && this == Tab.nullType)
				|| (this.kind == Struct.Kind.Arr && dest.kind == Struct.Kind.Arr
						&& dest.elemType.kind == this.elemType.kind)
				|| this == dest;
		*/
	}

	public boolean isNumeric() {
		return this.kind == Struct.Kind.Int;
	}

	public boolean isRefType() {
		return this.kind == Struct.Kind.Class || this.kind == Struct.Kind.Arr;
	}

	private boolean checkTypeNames(StructImpl typeSrc, StructImpl typeDest) {
		return typeSrc == typeDest;
	}
}

