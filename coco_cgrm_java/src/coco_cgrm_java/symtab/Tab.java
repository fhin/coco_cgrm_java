package coco_cgrm_java.symtab;

import coco_cgrm_java.impl.StructImpl;

/**
 * MicroJava Symbol Table
 */
public abstract class Tab {
	// Universe
	public static final StructImpl noType = new StructImpl(Struct.Kind.None);
	public static final StructImpl intType = new StructImpl(Struct.Kind.Int);
	public static final StructImpl charType = new StructImpl(Struct.Kind.Char);
	public static final StructImpl boolType = new StructImpl(Struct.Kind.Bool);
	public static final StructImpl nullType = new StructImpl(Struct.Kind.Class);

	public Obj noObj, chrObj, ordObj, putObj, putLnObj;

	/** The current top scope. */
	public Scope curScope;
	/** Nesting level of current scope. */
	public static int curLevel;

	public Tab() {
	}
}
