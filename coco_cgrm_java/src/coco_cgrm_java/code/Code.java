package coco_cgrm_java.code;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import coco_cgrm_java.code.Enums.*;
import coco_cgrm_java.code.Operand.OpdKind;
import coco_cgrm_java.symtab.Obj;
import coco_cgrm_java.symtab.Struct.Kind;
import coco_cgrm_java.symtab.Tab;

public class Code {	
	public static int none = -1;
	public static int any = -1;
	public static int pc = 0;
	
	private static int programStartAdr = 0;
	
	private static boolean[] free = {true,true,true,true,false,false,true,false};
	private static byte[] code = new byte[3000];
	
	public static void WriteInstToFile(String path){
		if (path == null || path.equals("")) return;
		try {
			PrintWriter pw = new PrintWriter(path);
			pw.close();
			FileOutputStream fos = new FileOutputStream(path);
			try {
				fos.write(code, 0, pc);
			} catch (IOException e) {
				System.out.println("Could not write to file");
			}
			try {
				fos.flush();
				fos.close();
			} catch (IOException e) {
				System.out.println("Could not close fileoutputstream " + e.getMessage());
			}
		} catch (FileNotFoundException e) {
			System.out.println("");
		}
	}
	
	public static void Put(int x){
		code[pc++] = (byte)x;
	}
	
	public static void Put2(int x){
		Put(x);
		Put(x >> 8);
	}
	
	public static void Put4(int x){
		Put2(x);
		Put2(x >> 16);
	}
	
	public static void PutConst(int size, int x){
		if (size == 1) Put(x);
		else if (size == 2) Put2(x);
		else Put4(x);
	}
	
	private static void Put4(int adr, int i) {
		code[adr] = (byte)i;
		code[++adr] = (byte)(i>>8);
		code[++adr] = (byte)(i>>16);
		code[++adr] = (byte)(i>>24);
	}

	private static int Get4(int adr) {
		int num = 0;
		for (int i = 3; i >= 0; i--){
			num <<= 8;
			num |= (code[adr+i]) & 0xFF;
		}
		return num;
	}
	
	public static void SetProgramStartAdr(){
		programStartAdr = pc;
		Put(Cmds.L_JMP.opCode);
		Put4(programStartAdr+1, 0);
		pc += 4;
	}
	
	public static void FixupStartAdr(int adr){
		int d = (adr - (programStartAdr+5));
		Put4(programStartAdr+1, d > 0 ? d : 0);
	}
	
	/**
	 * Retrieves the next free register and marks it as used if available
	 * @return The free register
	 */
	public static int GetReg(){
		int r = Reg.EAX.regNum;
		if (free[Reg.EBX.regNum]) r = Reg.EBX.regNum;
		else if (free[Reg.EDX.regNum]) r = Reg.EDX.regNum;
		else if (free[Reg.ECX.regNum]) r = Reg.ECX.regNum;
		else if (free[Reg.EAX.regNum]) r = Reg.EAX.regNum;
		else if (free[Reg.ESI.regNum]) r = Reg.ESI.regNum;
		// TODO: Add error logging
		//else Error("out of registers"); 
		free[r] = false;
		return r;
	}
	
	/**
	 * 
	 * @param reg
	 * @return
	 */
	public static int GetRegExclude(int reg){
		int r = Reg.EAX.regNum;
		boolean hasFreeReg = true;
	
		if (free[Reg.EBX.regNum] && reg != Reg.EBX.regNum) r = Reg.EBX.regNum;
		else if (free[Reg.EDX.regNum] && reg != Reg.EDX.regNum) r = Reg.EDX.regNum;
		else if (free[Reg.ECX.regNum] && reg != Reg.ECX.regNum) r = Reg.ECX.regNum;
		else if (free[Reg.EAX.regNum] && reg != Reg.EAX.regNum) r = Reg.EAX.regNum;
		else if (free[Reg.ESI.regNum] && reg != Reg.ESI.regNum) r = Reg.ESI.regNum;
		else hasFreeReg = false;
		if (hasFreeReg){
			return r;
		}
		else {
			// TODO: Add error logging
			//else Error("out of registers");
			return none;
		}
	}
	
	/**
	 * Marks the requested register if possible
	 * @param r The requested register
	 */
	public static void GetReg(int r){
		if (free[r]) {
			free[r] = false;
		}
		else {
			// Add error logging
			// Cannot allocate register r
		}
	}
	
	/**
	 * Checks if the requested register is used
	 * @param r The requested register
	 * @return If register is free true
	 */
	public static boolean isFree(int r){
		return free[r];
	}
	
	/**
	 * Releases the register, except for the registers EBP and ESP
	 * @param r The used register
	 */
	public static void FreeReg(int r){
		if (r == none) return;
		if (r != Reg.ESP.regNum && r != Reg.EBP.regNum && !free[r]){
			free[r] = true;
		}
		else {
			// Add error logging -> Msg: cannot deallocate register
		}
	}
	
	/**
	 * Frees all registeres except EBP and ESP
	 */
	public static void FreeAllRegs(){
		free[Reg.EAX.regNum] = true;
		free[Reg.EBX.regNum] = true;
		free[Reg.ECX.regNum] = true;
		free[Reg.EDX.regNum] = true;
		free[Reg.ESI.regNum] = true;
	}
	
	private static int GetRegForNum(int reg){
		if (reg < 0) return none;
		if (!isFree(reg) || reg == Reg.EBP.regNum || reg == Reg.ESP.regNum) return none;
		int register = none;
		for(Reg r : Reg.values()){
			if (r.regNum == reg) register = r.regNum;
		}
		return register;
	}
	
	public static Operand VarOpd(Obj obj){
		Operand o = new Operand();
		int adrOffset = 0;
		if (obj.level == 0){
			if (!Code.isReservedGloblProc(obj)){
				adrOffset  = 8; 
			}
			o.kind = OpdKind.RegRel;
			o.adr = obj.adr + adrOffset;
			o.reg = Reg.EDI.regNum;
			o.inx = none;
		}
		else {
			o.kind = OpdKind.RegRel;
			o.reg = Base(Tab.curLevel - obj.level);
			o.adr = obj.adr;
			o.inx = none;
		}
		if (obj.kind == Obj.Kind.VarPar){
			o.type = Tab.intType;
			Load(o);
			o.kind = OpdKind.RegRel;
		}
		o.type = obj.type;
		return o;
	}
	
	private static boolean isReservedGloblProc(Obj procObj){
		if (procObj == null) return false;
		if (procObj.name.equals("put") && procObj.nPars == 0) return true;
		else if (procObj.name.equals("putLn") && procObj.nPars == 1 && procObj.locals.size() == 1) return true;
		else return false;
	}
	
	public static int Base(int d){
		if (d == 0) return Reg.EBP.regNum;
		Operand sl = new Operand();
		sl.kind = OpdKind.RegRel;
		sl.reg = Reg.EBP.regNum;
		sl.adr = 8;
		sl.inx = none;
		sl.type = Tab.intType;
		while (d > 0){
			Load(sl);
			sl.kind = OpdKind.RegRel;
			sl.adr = 8;
			d--;
		}
		return sl.reg;
	}
	
	public static Operand ConOpd(int val){
		Operand o = new Operand();
		o.kind = OpdKind.Con;
		o.inx = none;
		o.type = Tab.intType;
		o.val = val;
		return o;
	}
	
	public static Operand RegOpd(int reg){
		Operand o = new Operand();
		o.kind = OpdKind.Reg;
		o.type = Tab.intType;
		o.adr = 0;
		o.inx = none;
		if (reg == any) o.reg = GetReg();
		else if (isFree(reg) && reg != Reg.EBP.regNum && reg != Reg.ESP.regNum){
			o.reg = GetRegForNum(reg);
		}
		else {
			o.reg = none;
			// TODO: error log
		}
		return o;
	}
	
	public static Operand CreateTmpOpd(int reg){
		Operand o = new Operand();
		o.kind = OpdKind.Reg;
		o.type = Tab.intType;
		o.adr = 0;
		o.inx = none;
		o.reg = reg;
		return o;	
	}
	
	public static Operand RegRelOpd(int reg, int adr){
		Operand o = new Operand();
		o.kind = OpdKind.RegRel;
		o.reg = reg;
		o.adr = adr;
		o.inx = none;
		return o;
	}
	
	public static void FreeOpd(Operand o){
		if (o.kind == OpdKind.Reg || o.kind == OpdKind.RegRel){
			FreeReg(o.reg);
			o.reg = none;
		}
		if (o.inx != none){
			FreeReg(o.inx);
			o.inx = none;
		}
	}
	
	/**
	 * Creates ModRM, SIB and Displacement Byte for a MOV operation to load 
	 * from the source register given into the destination register. 
	 * @param reg The destination register
	 * @param o The source register as described by its operand
	 */
	public static void PutOpds(int reg, Operand o){
		int rm = 0, mod = 0;
		// ModRm Byte
		if (o.inx == none){
			switch (o.kind){
			case Abs:
				mod = RegAdressingMode.MOD_NO_DISP.code;
				rm = RegAdressingMode.RM_D32_DISP.code;
				break;
			case Reg:
				mod = RegAdressingMode.ABS_ADR.code;
				rm = o.reg;
				break;
			case RegRel:
				assert (o.reg != Reg.ESP.regNum);
				mod = Mod(o.adr);
				rm = o.reg;
				break;
			default:
				break;
			}
		}
		else {
			//TODO Assert check
			assert (o.inx != Reg.ESP.regNum && 
					(o.kind == OpdKind.Abs || o.kind == OpdKind.RegRel));
			rm = RegAdressingMode.INDX_ADR.code;
			if (o.kind == OpdKind.Abs){
				mod = RegAdressingMode.REL_ADR.code;
				o.reg = Reg.EBP.regNum;
			}
			else {
				mod = Mod(o.adr);
			}
		}
		Put((mod << 6) + (reg << 3) + rm);
		// SIB Byte
		if (o.inx != none){
			Put((o.scale << 6) + (o.inx << 3) + o.reg);
		}
		// Disp
		if (mod == RegAdressingMode.MOD_NO_DISP.code && rm == RegAdressingMode.RM_D32_DISP.code) PutConst(4, o.adr); // Absolute adress
		else if (mod == RegAdressingMode.MOD_NO_DISP.code && rm == RegAdressingMode.INDX_ADR.code && o.reg == Reg.EBP.regNum){
			// Absolute indexed
			PutConst(4, o.adr);
		}
		else if (mod == RegAdressingMode.MOD_D8_DISP.code) PutConst(1, o.adr);
		else if (mod == RegAdressingMode.MOD_D32_DISP.code) PutConst(4, o.adr);
	}
	
	private static int Mod (int x){
		if (x == 0) return 0;
		else if (x >= -128  && x < 127) return 1;
		else return 2;
	}
	
	public static void PutMOV(Operand x, Operand y){
		assert x.type.size == y.type.size;
		int sizeFlag = PutPrefix(x);
		if (y.kind == OpdKind.Con){
			if (x.kind == OpdKind.Reg){
				// Move contents of immediate into register e.g, MOV r32, imm32 (if sizeFlag 1) else MOV r8, imm8
				// Prefix (4 Bit) + sizeBit for destReg (0 - 8 Bit, 1 - 32 Bit) + reg (3 Bit) + immediate
				Put(0xB0 + (sizeFlag << 3) + x.reg);
				PutConst(y.type.size, y.val);
			}
			else {
				// Move contents from immediate either 8bit or 32bit into source register with offset 8bit or 32bit
				Put(0xC6 + sizeFlag); PutOpds(0, x);
				PutConst(y.type.size, y.val);
			}
		}
		else if (x.kind == OpdKind.Reg){
			// MOV r32, r/m32 (Move from dest reg. [with offset given by /m32 or /m8] into source reg]
			Put(0x8A + sizeFlag);
			PutOpds(x.reg, y);
		}
		else if (y.kind == OpdKind.Reg){
			// MOV r/m32, r32 (Move from dest reg. into source reg [with offset given by /m32 or /m8])
			Put(0x88 + sizeFlag);
			PutOpds(y.reg, x);
		}
		else {
			// TODO: Error (...)
		}
	}
	
	private static int PutPrefix(Operand x){
		 if (x.type.size == 1) return 0;
		 else if (x.type.size == 2) {
		  	Put(0x66); return 1;
		 }
		 else if (x.type.size == 4) return 1;
		 else { 
			// Error(...); 
			 return 0;
		 }
	}
	
	public static void PutDyadic (int op, Operand x, Operand y) {
		int sf = PutPrefix(x);
		if (x.kind == OpdKind.Reg && x.reg == Reg.EAX.regNum && y.kind == OpdKind.Con && x.type.size == y.type.size) {
			// EAX := EAX op imm
			Put(op + 4 + sf); 
			PutConst(y.type.size, y.val);
		} else if (y.kind == OpdKind.Con) {
			if (x.type.size > 1 && -128 <= y.val && y.val <= 127) { // rm := rm op signextend(imm8)
				Put(0x82 + sf); 
				PutOpds(op/8, x); 
				PutConst(1, y.val);
			} else if (x.type.size == y.type.size) { // rm := rm op imm
				Put(0x80 + sf);
				PutOpds(op/8, x); 
				PutConst(y.type.size, y.val);
			}
			// else TODO: Error logging Error(...);
		} else if (x.kind == OpdKind.Reg) { // r := r op rm
			Put(op + 2 + sf); 
			PutOpds(x.reg, y);
		} else if (y.kind == OpdKind.Reg) { // rm := rm op r
			Put(op + sf); 
			PutOpds(y.reg, x);
		} //else Error(...);
		FreeOpd(y);
	}
	
	/**
	 * Loads the given operand in a register
	 * e.g MOV EAX, -24[EBP + 3*ESI]
	 * @param x The operand to load
	 */
	public static void Load (Operand x) {
		Operand r = null;
		switch (x.kind) {
			case Reg: return;
			case RegRel: case Con: case Abs:
				if (x.kind == OpdKind.RegRel && x.reg != Reg.EBP.regNum && x.reg != Reg.EDI.regNum){
					r = RegOpd(x.reg);
				}
				else {
					r = RegOpd(any);
				}
				r.type = x.type; 
				PutMOV(r, x); 
				FreeReg(x.inx);
			    break;
			default:
				// Error(...);
				break;
		}
		x.kind = OpdKind.Reg; x.reg = r.reg; x.adr = 0; x.inx = none;
	}
	
	/**
	 * Loads the adress of an operand into a register, e.g load the adress of a VAR parameter
	 * @param x
	 */
	public static void LoadAdr (Operand x) {
		if (x.kind == OpdKind.RegRel && x.adr == 0 && x.inx == none) {
			x.kind = OpdKind.Reg;
		} else if (x.kind == OpdKind.RegRel || x.kind == OpdKind.Abs) {
			Operand r;
			if (x.kind == OpdKind.Abs || x.reg == Reg.EBP.regNum){
				r = RegOpd(any);
			}
			else { // x.kind == Opd.RegRel && x.reg != EBP
				r = RegOpd(x.reg);
			}
			PutLEA(r, x);
			FreeReg(x.inx);
			x.kind = OpdKind.Reg; x.reg = r.reg; x.adr = 0; x.inx = none;
		} 
		else  {
		//	Error(...); // Reg, Con, Proc
		}
	}
	
	/**
	 * Loads the effective register given from the operand into the destination register
	 * @param dest The destination register operand
	 * @param src The source register operand
	 */
	public static void PutLEA(Operand dest, Operand src){
		assert dest.kind == OpdKind.Reg;
		Put(0x8D);
		assert (src.kind == OpdKind.Abs || src.kind == OpdKind.Reg || src.kind == OpdKind.RegRel);
		PutOpds(dest.reg, src);
	}
	
	/**
	 * Adepts the offset of the operand for the given field
	 * @param x The operand containing the record object where the field should be accessed
	 * @param fld The field obj
	 */
	static void GenField (Operand x, Obj fld) {
		assert(x.kind == OpdKind.RegRel || x.kind == OpdKind.Abs);
		x.adr += fld.adr;
		x.type = fld.type;
	}
	
	static void GenIndex (Operand x, Operand i) {
		assert x.type.kind == Kind.Arr;
		int s = x.type.elemType.size;
		if (i.kind == OpdKind.Con) {
			if (i.val < 0 || i.val >= x.type.n){
				// TODO: Error
			}
			x.adr += s * i.val;
		}
		else {
			LoadAdr(x); Load(i);
			Operand limit = ConOpd(x.type.n);
			PutDyadic(DyadicOps.CMP.opCode, i, limit);
			int adr = 0;
			ArrayList<Integer> tmp = new ArrayList<Integer>();
			tmp.add(adr);
			tmp = PutJcc(CmpOps.LT.idx, CondHelper.TJMP.val, CondHelper.SGND.val, tmp);
			adr = tmp.get(0);
			// TODO: PutINT3();
			Fixup(adr);
			if (s == 1 || s == 2 || s == 4 || s == 8) {
				x.kind = OpdKind.RegRel; x.inx = i.reg; x.scale = Scale(s);
			} else {
				Operand scale = ConOpd(s); 
				PutMUL(i, scale); 
				PutDyadic(DyadicOps.ADD.opCode, x, i);
				x.kind = OpdKind.RegRel;
			}
		}
		x.type = x.type.elemType;
	}
	
	static int Scale (int s) {
		if (s == 1) return 0;
		else if (s == 2) return 1;
		else if (s == 4) return 2;
		else return 3;
	}
	
	/**
	 * Negates a number by replacing the numbers value with its two complement
	 * The operand can either be a register or memory location
	 * @param x
	 */
	public static void PutNEG(Operand x){
		if (x.kind == OpdKind.Proc || x.kind == OpdKind.Cond){
			// TODO: Error logging
			return;
		}
		
		int sf = PutPrefix(x);
		Put(Cmds.NEG.opCode + sf);
		PutOpds(x.reg, x); // TODO:
	}
	
	/**
	 * Performs a signed multiplication in the following formats:
	 * 
	 * IMUL EAX, EAX * src (One operand form)
	 * IMUL dest, src * dest (Two operand form)
	 * IMUL dest, src * src2 (Three operand form)
	 * @param src An operand representing the register of an src operand
	 * @param dest An operand representing the register where the result of the multiplication is stored
	 * @param src2 A second src operand only used in the three operand form
	 */
	public static void PutIMUL(Operand src, Operand dest, Operand src2){
		int opCode = 0xF6;
		if (src2 == null){
			if (dest == null){
				// One operand form
				int s = PutPrefix(src);
				if (src.kind == OpdKind.Reg){
					if (src.reg == Reg.EAX.regNum){
						// Error
					}
					else {
						Put(opCode + s);
						Put((0xE8 << 5) + src.reg);
					}
				}
				else {
					Put(opCode + s);
					int reg = src.reg;
					src.reg = Reg.EAX.regNum;
					PutOpds(reg, src);
					src.reg = reg;
				}
			}
			else {
				// Two operand form with either 
				if (dest.kind != OpdKind.Reg) {
					// Error
					return;
				}
				if (src.kind == OpdKind.Con){
					if (src.val > 127 || src.val < -128){
						opCode = 0x69; // No sign extension of immediate data
					}
					else {
						opCode = 0x6B; // Sign extension of immediate data
					}
					Put(opCode);
					Put((3 << 6) + (dest.reg << 3) + dest.reg);
					Put(src.val);
				}
				else {
					Put(0x0F);		// Put two byte opcode prefix
					Put(0xAF);		
					if (src.kind == OpdKind.Reg){
						Put(3);
						Put((dest.reg << 3) + (src.reg));
					}
					else {
						Code.PutOpds(dest.reg, src);
					}
				}
			}
		}
		else {
			// Three operand form
			if (src2.kind != OpdKind.Con || dest.kind != OpdKind.Reg) {
				// Error
			}
			if (src2.val > 127 || src2.val < -128){
				opCode = 0x6B; // No sign extension of immediate data
			}
			else {
				opCode = 0x69;
			}
			Put(opCode);
			if (src.kind == OpdKind.Reg){
				Put((3 << 6) + (dest.reg << 3) + src.reg);
				Put(src2.val);
			}
			else {
				PutOpds(dest.reg, src);
				Put(src2.val);
			}
		}
	}

	/**
	 * Performs a signed division with the quotient stored in the EAX and the remainder 
	 * in the EDX register
	 * @param dividend The operand describing the dividend 
	 * @param divisor The operand describing the divisor can either be a general purpose register or
	 * a memory location
	 */
	public static void PutIDIV(Operand dividend, Operand divisor, boolean isModOp){
		Operand divisorReg = null;
		if (divisor.kind == OpdKind.Cond || divisor.kind == OpdKind.Proc){
			// Error
		}
		
		Operand dividendReg = Code.CreateTmpOpd(Reg.EAX.regNum);
		if (dividend.reg != Reg.EAX.regNum){
			PutMOV(dividendReg, dividend); // Move divdend in EAX register
		}
		if (divisor.kind == OpdKind.Con){
			divisorReg = Code.RegOpd(Code.GetRegExclude(Reg.EDX.regNum));
			PutMOV(divisorReg, divisor);
		}
		PutPrefix(dividendReg);
		Put(Cmds.CWD.opCode); // Sign extend EAX register into EDX register
		int s = PutPrefix(divisor);
		Put(Cmds.IDIV.opCode + s);
		
		if (divisor.kind == OpdKind.Reg){
			Put(0xE8 + divisor.reg);
		}
		else {
			if (divisor.kind == OpdKind.Con){
				if (divisorReg != null){
					Code.PutOpds(Reg.EDI.regNum, divisorReg);
				}
			}
			else {
				Code.PutOpds(Reg.EDI.regNum, divisor);
			}
		}
		if (!isModOp){
			PutMOV(dividend, Code.CreateTmpOpd(Reg.EAX.regNum));
		}
		else {
			PutMOV(dividend, Code.CreateTmpOpd(Reg.EDX.regNum));
		}
	}
	
	/**
	 * Performs a bitwise local AND operation on the destination and src operand and stores the 
	 * result in the destination.
	 * @param dest The operand describing the destination (register or memory location)
	 * @param src The operand describing the src (immediate, register or memory location)
	 */
	public static void PutLogicAND(Operand dest, Operand src){
		if (dest.kind == OpdKind.Reg || dest.kind == OpdKind.RegRel){
			int s = PutPrefix(src);
			switch(src.kind){
			case Con:
				if (dest.kind == OpdKind.Reg && dest.reg == Reg.EAX.regNum){
					Put(Cmds.AND_IMM_TO_EAX.opCode + s);
				}
				else if (dest.kind != OpdKind.Reg){
					Put(Cmds.AND_IMM.opCode + s);
					PutOpds(dest.reg, dest);
				}
				else {
					Put(Cmds.AND_IMM.opCode + s);
					Put((3 << 6) + (4 << 3) + dest.reg);
				}
				PutConst(src.type.size, src.val);
				break;
			case Reg: 
				if (dest.kind == OpdKind.Reg){
					Put(Cmds.AND_REG_TO_MEM.opCode + s);
					Put((3 << 6) + (src.reg << 3) + dest.reg);
				}
				else {
					Put(Cmds.AND_REG_TO_MEM.opCode + s);
					PutOpds(dest.reg, src);
				}
				break;
			case RegRel:
			case Abs:
				if (dest.kind == OpdKind.Reg){
					Put(Cmds.AND_MEM_TO_REG.opCode + s);
					PutOpds(dest.reg, src);
				}
				break;
			default:
				break;
			}		
		}
	}
	
	public static boolean isPowerOfTwo(Operand x){
		if (x.kind != OpdKind.Con) return false;
		return x.val % 2 == 0;
	}
	
	/**
	 * Performs the modulo operation (src % modulo) on the given operands.
	 * If the modulo operand is a constant and a power of two the result will be calculated
	 * by the following expression: src & (modulo - 1).
	 * The result of the modulo operation will be stored in the src operand 
	 * @param src
	 * @param modulo 
	 */
	public static void PutMODULO(Operand src, Operand modulo){
		Operand tmpReg = null;
		if (modulo.kind == OpdKind.Proc || modulo.kind == OpdKind.Cond){
			 // Error
		}
		else if (modulo.kind == OpdKind.Con){
			if (isPowerOfTwo(modulo)){
				// TODO: a % b && b = (x*2) => a % (x*2) = a & (b-1)
			}
			else {
				tmpReg = RegOpd(GetReg());
				Code.PutMOV(tmpReg, modulo);
			}
		}
		PutIDIV(src, modulo, true);
		if (src.reg != Reg.EDX.regNum){
			PutMOV(src, CreateTmpOpd(Reg.EDX.regNum));
		}
	}
	
	public static void PutMUL(Operand idx, Operand scale){
		Operand mulReg = CreateTmpOpd(Reg.EAX.regNum);
		if (idx.reg != Reg.EAX.regNum){
			PutMOV(mulReg, idx); // MOV EAX, IDX
		}
		// Put MUL
		int mulOpCode = scale.val < 256 ? Cmds.MUL_1B.opCode : Cmds.MUL.opCode; 
		assert idx.kind == OpdKind.Reg;
		
		// TODO: Maybe rewrite to more readable format
		Put(mulOpCode);
		Put((3 << 6) + (4 << 3) + idx.reg); 
	}
	
	 public static void GenOp (int opC, Operand x, Operand y) {
		 final Ops op = getOpForCode(opC);
		 if (x.kind == OpdKind.Con && y.kind == OpdKind.Con) {
			 switch(op) {
			 	case ADD: x.val += y.val; break;
			 	case MINUS: x.val -= y.val; break;
			 	case MUL: x.val *= y.val; break;
			 	case DIV: x.val /= y.val; break;
			 	case MOD: x.val %= y.val; break;
			 	default: break;
			 }
		 } else {
			 Load(x);
			 switch(op) {
			 	case ADD: PutDyadic(DyadicOps.ADD.opCode, x, y); break;
			 	case MINUS: PutDyadic(DyadicOps.SUB.opCode, x, y); break;
			 	case MUL: PutIMUL(y, x, null); break;
			 	case DIV: PutIDIV(x, y, false); break;
			 	case MOD: PutMODULO(x, y); break;
			 	default: break;
			 }
		 }
	 }
	 
	 private static Ops getOpForCode(int opC){
		 for (Ops op : Ops.values()){
			 if (op.ascii_val == opC) return op;
		 }
		 return Ops.NONE;
	 }
	 
	 public static int PutJMP (int adr) {
		 if (adr > 0) { // backward jump
			 int d = (pc + 2) - adr;
			 if (d <= 128) {
				 Put(Cmds.S_JMP.opCode); PutConst(1, -d);
			 } else {
				 Put(Cmds.L_JMP.opCode); PutConst(4, -(d+3));
			 }
		 } 
		 else { // forward jump
			 Put(Cmds.L_JMP.opCode); PutConst(4, adr);
			 adr = - (pc - 4);
		 }
		 return adr;
	 }
	 
	 // TODO: Probably remove this function
	 public static void Fixup (int adr) {
		 while (adr < 0) {
			 adr = -adr;
			 int x = Get4(adr);
			 Put4(adr, pc - (adr + 4));
			 adr = x;
		 }
	}
	 
	 public static ArrayList<Integer> Fixup(ArrayList<Integer> elem){
		 if (elem == null || elem.size() == 0){
			 // error logging
			 return elem;
		 }
		 int adr = elem.get(elem.size()-1);
		 while (adr < 0){
			int tmp_val = -adr;
			int x = Get4(tmp_val); // Retrieve link to next fixup cell
			Put4(tmp_val, pc - (tmp_val + 4)); // Update jump adress for current cell
			adr = x;
		 }
		 return elem;
	 }
	 
	 // op = const int eq = 0, neq = 1, lt = 2, geq = 3, gt = 4, leq = 5;
	 public static ArrayList<Integer> PutJcc (int op, boolean neg, boolean signed, ArrayList<Integer> elem) {
		 if (signed) op = Enums.jmpOpC[op]; else op = Enums.jmpOpC[op+6];
		 if (neg) op = op ^ 1;
		 //TODO: Check for correct implementation
		 int adr = elem.get(elem.size()-1);
		 if (adr > 0) { // backward jump
			 int d = (pc + 2) - adr;
			 if (d <= 128) {
				 Put(op); PutConst(1, -d);
			 } else {
				 Put(0x0F); Put(op + 0x10); PutConst(4, -(d+4));
			 }
		 } 
		 else { // forward jump
			 Put(0x0F); Put(op + 0x10); PutConst(4, adr);
			 elem.set(elem.size()-1, -(pc -4));
		 }
		 return elem;
	 }
	 
	 /**
	  * Updates the given jump lists of list_1 to the ones of list_2
	  * @param list_1 The jump list to be updated
	  * @param list_2 The target jump list head element
	  * @return The updated jump list
	  */
	 public static ArrayList<Integer> Append (ArrayList<Integer> list_1, ArrayList<Integer> list_2) {
		 if (list_1 == null) return list_2;
		 else if (list_2 != null){
			 int newAdr = list_1.get(list_1.size()-1);
			 int adr = list_2.get(0);
			 if (adr > 0){
				 Put4(adr, newAdr); // Link adress of old list to last cell of first list
				 adr = -adr;
				 list_2.set(0, adr);
			 }
			 list_1.addAll(list_2);
			 
		 }
		return list_1;
	 }
	 
	 /**
	  * Pushes the given operand onto the stack
	  * @param o The operand to push
	  */
	 public static void PutPUSH(Operand o){
		 PutPrefix(o);
		 switch(o.kind){
		 case Con:
			 int s = (o.val > 127 || o.val < -128)  ? 0 : 2;
			 Put(Cmds.PUSH_IMM.opCode + s);
			 // Stack can only be 16bit or 32bit aligned
			 // For parameter retrieval add offset of 1 for 16 bit / 3  for 32 bit alignment
			 PutConst(o.type.size, o.val);
			 break;
		 case Reg: 
			 Put(Cmds.PUSH_REG.opCode + o.reg);
			 break;
		 case Abs:
		 case RegRel:
			 Put(Cmds.PUSH_MEM.opCode);
			 Code.PutOpds(Reg.ESI.regNum, o);
			 break;
		 default:
			 // TODO: Error handling
			 break;
		 }
	 }
	 
	 public static void PutPOP(int reg){
		 Put(Cmds.POP.opCode);
		 Put((3 << 6) + (0 << 3) + reg);
	 }
	 
	 public static void PutCALL(int relAdr){
		 Put(Cmds.CALL.opCode);
		 PutConst(Tab.intType.size, relAdr);
	 }
	 
	 public static void PutCALLGlobal(Obj procObj){
		 if (procObj == null) return;
		 Put(Cmds.CALL_INDIRECT.opCode);
		 Operand globProcOp = Code.RegRelOpd(Reg.EDI.regNum, procObj.adr); // Load adress of global procedure
		 PutOpds(Reg.EDX.regNum, globProcOp); 
	 }
	 
	/**
	 * Create a new stack frame for the called procedure.
	 * Should be called after the procedures epilogue is done, meaning after a CALL
	 * @param procSize The stack frame of the procedure, normally the size of the local variables
	 * @param nestingLvl The nesting level of the procedure
	 */
	 public static void PutENTER(int procSize, int nestingLvl){
		 Put(Cmds.ENTER.opCode);
		 Put2(procSize);
		 Put(nestingLvl);
	 }
	 
	 /**
	  * Computes and pushes the static link in the Prolog of a procedure call.
	  * Parameters are assumed to be pushed onto the stack beforehand.
	  * @param proc
	  */
	 public static void Call(Obj proc, int callerLvl, boolean isPredefined){
		 if (proc.level == 0){
			 if (isPredefined){
				 // Call to global predefined method, e.g. Put() or PutLn()
				 Code.PutCALLGlobal(proc);
			 }
			 else {
				// Global procedure that is not predefined: do nothing
				 PutCALL(proc.adr - (pc + 5));
			 }
		 }
		 else {
			 if (proc.level > callerLvl){ // Call inner procedure
				 Operand sl = RegOpd(Reg.EBP.regNum);
				 PutPUSH(sl);
			 }
			 else { // Call outer procedure: proc.level <= callerLevel
				 Operand sl = RegRelOpd(Reg.EBP.regNum, 8);
				 sl.type = Tab.intType;
				 int d = callerLvl - proc.level;
				 while (d > 0){
					 Load(sl);
					 sl.kind = OpdKind.RegRel;
					 sl.adr = 8;
					 d--;
				 }
				 PutPUSH(sl);
		 	}
		 	PutCALL(proc.adr - (pc + 5));
		 }
	 }
	 
	 /**
	  * With the LEAVE instruction the frame pointer will be move into the stack pointer register, 
	  * which will release the stack frame allocated by the ENTER instruction at the start
	  * of the method call. The old frame pointer will be popped from the stack (saved by the Enter
	  * instruction) into the EBP register.
	  */
	 public static void PutLEAVE(){
		// Put(0x66);
		 Put(Cmds.LEAVE.opCode);
	 }
	 
	 /**
	  * Transfers program control to a return address located on the top of the stack. 
	  * The address is usually placed on the stack by a CALL instruction, 
	  * and the return is made to the instruction that follows the CALL instruction.
	  * With the method parameter one can specify how many bytes will be released, 
	  * from the stack, after the return adress is popped.
	  * @param paramSize The number of bytes to be released from the stack after the return adress is popped
	  */
	 public static void PutRET(int paramSize){
		// Put(0x66);
		 Put(Cmds.RET.opCode);
		 Put2(paramSize);
	 }
	 
	 public static Operand CallGlobInlineProc(Operand paramOp, Obj globInlineProc){
		 Operand x = Code.RegOpd(any);
		 if (paramOp == null || globInlineProc == null) return x;
		 else if (paramOp.kind != OpdKind.Reg){
			 Load(paramOp);
		 }
		 if (globInlineProc.name.equals("ORD")){
			 paramOp.type = Tab.intType;
		 }
		 else if (globInlineProc.name.equals("CHR")){
			 paramOp.type = Tab.charType;
		 }
		 return paramOp;
	 }
	 
	 public static void Close(){
		 Put(Cmds.RET.opCode + 1);
	 }
}
