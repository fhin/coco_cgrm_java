package coco_cgrm_java.code;

import java.util.ArrayList;

import coco_cgrm_java.impl.StructImpl;

public class Operand {
	public enum OpdKind {
		Con, Abs, Reg, RegRel, Proc, Cond
	}
	
	public OpdKind kind;
	public StructImpl type;
	
	public int lastCmpOp;
	public ArrayList<Integer> tJmpL;
	public ArrayList<Integer> fJmpL;
	public int level; // Lvl: 0 global otherwise local
	public int val; // Constant value
	public int reg; // Register for Reg and RegRel operand
	public int adr; // RegRel: offset, Abs: adress
	public int inx; // Abs, RegRel: index register if not none
	public int scale; // Abs, RegRel: scale factor if inx != none
}
